class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        -If positive always add
        -If negative, it starts branching. Include previous as long as >0, once gets to <0 reset
        *fails for all negative, default starting max is first element, if max<0 then compares to current element
        alternate
        - track max including and max excluding
        -Max up to current point
        """
        maxSum = nums[0]
        currentSum = maxSum
        for i in range(1,len(nums)):
            if maxSum<0:
                if nums[i]>maxSum:
                    maxSum = nums[i]
                    currentSum = maxSum
            else:
                if nums[i]>=0:
                    if currentSum>0:
                        currentSum += nums[i]
                    else:
                        currentSum = nums[i]
                else:
                    currentSum+=nums[i]
                    
                if currentSum>maxSum:
                    maxSum = currentSum
        return maxSum